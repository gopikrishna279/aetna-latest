import {
  Component,
  Inject,
  OnInit
} from '@angular/core';
import * as _ from 'lodash';

import { ThreadsService } from './../thread/threads.service';
import { MessagesService } from './../message/messages.service';

import { Thread } from './../thread/thread.model';
import { Message } from './../message/message.model';

@Component({
  selector: 'appliedclaims',
  templateUrl: './appliedclaims.html',
  styleUrls: ['./appliedclaims.css']
})
export class appliedclaimsComponent { }
