import {
  Component,
  Inject,
  OnInit
} from '@angular/core';
import { baWizzardComponent } from '../../shared/baWizzard/baWizzard.component';
import { baWizzardStepComponent } from '../../shared/baWizzard/baWizzardStep.component';
import * as _ from 'lodash';

import { ThreadsService } from './../thread/threads.service';
import { MessagesService } from './../message/messages.service';

import { Thread } from './../thread/thread.model';
import { Message } from './../message/message.model';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { SharedService } from '../shared.service';


@Component({
  selector: 'claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.css']
})
export class ClaimComponent implements OnInit{

  public form:FormGroup;
  public cid:AbstractControl;
  public fname:AbstractControl;
  public lname:AbstractControl;
  public pcode:AbstractControl;
  public illness:AbstractControl;
  public age:AbstractControl;
  public rfv:AbstractControl;
  public doi:AbstractControl;
  public emer:AbstractControl;
  public dfc:AbstractControl;
  public dos:AbstractControl;
  public tos:AbstractControl;
  public pos:AbstractControl;
  public pci:AbstractControl;
  public dc:AbstractControl;
  public validationbtn:boolean=false;
  diagnosis: number;
  timer;
  procedure: number;
  private _isCompleted: boolean = false;
  private iswarning: boolean = false;
  private iswarning1: boolean = false;
  public procedurecode:boolean=false;

  public cellbackground: boolean = false;
  public warning;
  public warning1;

  public typeofservice;
  typeservice;
  placeserice;
  memid: number;
  firstname;
  public placeofservice;
  public service:boolean = false;
  public reason;
  dateofillness;
  dateoffirstconsult;
  similarillness;
  i=0;
  checkbox = [];
  /* details= [];
 data1;
 data2;*/
  emers;
  public success:boolean=false;
  emergency;
  /* public show:boolean=false;
 public valid:boolean=false;
   public valid1:boolean=false;
   public valid2:boolean=false;
 public  next_step:boolean;
 public all_next_steps:boolean;*/
  constructor(
    public fb: FormBuilder,
    private sharedsevice: SharedService
  ) {

    this.form = fb.group({
      'cid': ['', Validators.required],// Validators.compose([Validators.required])
      'fname': ['', Validators.required],
      'lname': ['', Validators.required],
      'pcode': ['', Validators.required],
      'age': ['', Validators.required],
      'rfv': ['', Validators.required],
      'doi': ['', Validators.required],
      'emer': ['', Validators.required],
      'dfc': ['', Validators.required],
      'illness': ['', Validators.required],
      'dos': ['', Validators.required],
      'tos': ['', Validators.required],
      'pos': ['', Validators.required],
      'pci': ['', Validators.required],
      'dc': ['', Validators.required],

    });
    this.cid = this.form.controls['cid'];
    this.fname = this.form.controls['fname'];
    this.lname = this.form.controls['lname'];
    this.pcode = this.form.controls['pcode'];
    this.age = this.form.controls['dob'];
    this.rfv = this.form.controls['rfv'];
    this.doi = this.form.controls['doi'];
    this.dfc = this.form.controls['doi'];
    this.illness = this.form.controls['doi'];
    this.dos = this.form.controls['doi'];
    // this.emer = this.form.controls['emer'];
    this.tos = this.form.controls['tos'];
    this.pos = this.form.controls['pos'];
    this.pci = this.form.controls['pci'];
    this.dc = this.form.controls['dc'];
  }





  ngOnInit() {

    let that=this;
    var i=1;
    $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='name\"+i+\"' type='date' placeholder='date' class='form-control input-md'   formControlName='dos'  /></td><td><input name='name"+i+"' type='text' placeholder='Type of Service' class='form-control input-md' disabled /> </td><td><input  name='mail"+i+"' type='text' placeholder=' Place of Service'  class='form-control input-md' disabled></td><td><input  name='mobile"+i+"' type='text' placeholder='Procedure Code Identify'  class='form-control input-md'></td></td><td><input name='name\"+i+\"' type='text' placeholder=' Diagnosis Code' class='form-control input-md'  /></td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');


      i++;
    });
    $("#delete_row").click(function(){
      if(i>1){
        $("#addr"+(i-1)).html('');
        i--;
      }
    });




    this.myCallback();

    this.timer = setInterval(() => {
      this.myCallback();
    }, 1500);


  }


  myCallback() {

    if(this.procedure==52317) {

      this.sharedsevice.insertData('52317: Kidney problem : Litholapaxy: crushing or fragmentation of calculus by any means in bladder and removal of fragments; simple or small (less than 2.5 cm)');

    }

    if(this.procedure==87077) {

      this.sharedsevice.insertData("87077:Related Throat problem : Superficial wounds, abscess, aspirates, animal bites. Skin biopsy should be submitted in a small amount of sterile, nonbacteriostatic saline.");

    }

    if(this.procedure==89900) {
      this.procedurecode=true;
      this.sharedsevice.insertData("89900: procedure code does not exist");

    }
    else{
      this.procedurecode=false;
    }



    if (this.diagnosis == 290.43 && Number(this.age)  < 65 && this.procedure == 52317) {

      this.success  = false;
      console.log("entered");
      const msg = "Note: 290.43 represents Vascular dementia which frequentely occurs in age above 65 years old.";
      this.sharedsevice.insertData(msg);
      this.cellbackground = true;
      this.validationbuttotn();

    }

    else if (this.diagnosis == 290.43 && Number(this.age)  < 65 && this.procedure == 87077 ) {
      this.success  = false;
      console.log("entered");
      const msg = "Note: 290.43 represents Vascular dementia which frequentely occurs in age above 65 years old";
      this.sharedsevice.insertData(msg);

      this.cellbackground = true;
      this.validationbuttotn();

    }



    else if (this.diagnosis == 290.43 && this.procedure == 87077) {

      this.success  = false;
      console.log("entered");
      const msg = "Conflict: 87077 is the procedure code related to throat problems but diagnosis code 290.43 related to brain disease."
      this.sharedsevice.insertData(msg);

      this.cellbackground = true;

      this.validationbuttotn();

    }

    else if (this.diagnosis == 290.43 && this.procedure == 52317) {

      this.success  = false;
      console.log("entered");
      const msg = "Conflict: 52317 is the procedure code related to  kidney stones but diagnosis code 290.43 related to brain disease."
      this.sharedsevice.insertData(msg);
      this.cellbackground = true;
      this.validationbuttotn();

    }



    else {
      this.cellbackground = false;
    }

    /* if(this.typeservice == '2' && this.placeserice == 'H' ){
       let msg="Conflict: Type of Service(2) is surgery and Place of service(H) is patient home, Not possible perform surgery at patient home";
        this.sharedsevice.insertData(msg);
        this.service = true;
        this.validationbuttotn();
      }
      else {
       this.service = false;
     }
  */

    if(this.typeofservice == '2' && this.placeofservice  == 'H' ){
      let msg = "Conflict: Type of Service(2) is surgery and Place of service(H) is patient home, Not possible perform surgery at patient home";
      this.sharedsevice.insertData(msg);
      this.service = true;
      this.validationbuttotn();

      this.success  = false;
    }
    else {
      this.service = false;
    }





//tick mark

    if (this.typeofservice !== '2' && typeof this.typeofservice !== 'undefined' && typeof this.placeofservice !== 'undefined'  && this.placeofservice !== 'H'  && this.procedure !== 52317 && this.procedure !== 87077  && typeof this.procedure !== 'undefined'  && this.diagnosis == 290.43) {
      this.success = true;

    }
    else {
      this.success  = false;
    }
    /*if (this.typeofservice !== '2' && typeof this.typeofservice !== 'undefined' && typeof this.placeofservice !== 'undefined'  && this.placeofservice !== 'H'  && this.procedure !== 87077  && this.diagnosis == 290.43) {
      this.success = true;

    }
    else {
      this.success  = false;
    }*/

  }

  validation(value) {
    this.success  = false;  //false

    this.service = false;
    //this.validationbtn = true;
    this.cellbackground = false; //false
    this.iswarning = false;
    this.validationbtn = true; //true
    this.warning1 = value;
    if(this.warning1=='no'){
      this.iswarning1 = true;
      this.sharedsevice.insertData('no');

    }
    else {
      this.iswarning1 = false;
    }


  }


  validation1(value) {
    this.service = false; //false
    this.success  = false;

    this.cellbackground = false;
    this.iswarning1 = false;
    this.validationbtn = true;
    this.warning= value;

    if(this.warning == 'yes'){
      this.iswarning = true;
    }
    else {
      this.iswarning = false;
    }

  }



  validationbuttotn(){
    if ( this.validationbtn) {
      this.cellbackground = false;
    }
  }

  idstore(values) {
    this.cid = values.cid;
    console.log(this.cid);
    this.memid = Number(this.cid);

    if(this.memid  == 5236 && this.firstname !== 'gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "first name does not matched with member id." );
    }
    if(this.memid  == 5236 && this.firstname !== 'Gopi' && typeof this.firstname !== "undefined"){
      this.sharedsevice.insertData("first name does not matched with member id."  );
    }

    if(this.memid  == 5236 && this.firstname == 'gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "Hey, first name  matched with member id." );
    }

    if(this.memid  == 5236 && this.firstname == 'Gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "Hey, first name  matched with member id." );
    }


    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }


  }
  agestore(values) {
    this.age = values.age;
    console.log(this.age );

    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }


  }
  firststore(values){

    this.fname = values.fname;
    console.log(this.fname );
    this.firstname = this.fname;


    if (this.memid  == 5236 && this.firstname !== 'gopi' && typeof this.firstname !== "undefined" ) {
      this.sharedsevice.insertData( "first name does not matched with member id." );
    }

    if(this.memid  == 5236 && this.firstname !== 'Gopi' && typeof this.firstname !== "undefined"){
      this.sharedsevice.insertData( "first name does not matched with member id.");
    }


    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }

    if(this.memid  == 5236 && this.firstname == 'gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "Hey, looks good first name  matched with member id." );
    }

    if(this.memid  == 5236 && this.firstname == 'Gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "Hey, looks good first name  matched with member id" );
    }


  }
  laststore(values){
    this.lname = values.lname;
    console.log(this.lname );


    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }

    console.log(this.memid);

    console.log(this.firstname);
    console.log(this.age);
    console.log(this.lname);
    console.log(this.rfv);
  }
  reasonstore(values) {
    this.rfv = values.rfv;
    console.log(this.rfv );
    this.reason = this.rfv;

    if (this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi')&& typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }



  }


  //second step validation
  secondstep(){
    if(typeof this.dateofillness !== "undefined" && typeof this.emergency !== "undefined" && typeof  this.dateoffirstconsult !== "undefined" && typeof this.similarillness !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated second step. you just one step away complete.");
    }
  }



  doistore(values) {
    this.doi = values.doi;
    console.log(this.doi );

    this.dateofillness = this.doi;
    this.secondstep();

  }

  emerstore(values) {
    this.emers = values;
    console.log(this.emers );

    if ( this.emers  ===  1 ) {
      this.emergency = 'Yes';
      console.log( this.emergency);
    } else if (this.emers  ===  2){
      this.emergency = 'No';
      console.log( this.emergency);
    }

    this.secondstep();
  }

  dfcstore(values) {
    this.dfc=values.dfc;
    console.log(this.dfc );
    this.dateoffirstconsult = this.dfc;

    this.secondstep();
  }
  illnesstore(values){
    this.illness= values.illness;
    console.log(this.illness );
    this.similarillness = this.illness;

    this.secondstep();
  }

  checkstore(values){
    this.checkbox.push(values);
  }
  dosstore(values) {
    console.log("hai");

    this.dos = values.dos;
    console.log(this.dos);


  }
  tosstore(values) {
    console.log("hai");

    this.tos = values.tos;
    console.log(this.tos);
    this.typeservice = this.tos;
  }

  posstore(values) {
    console.log("hai");

    this.pos = values.pos;
    console.log(this.pos);
    this.placeserice = this.pos;
  }

  pcistore(values) {
    console.log("hai");
    this.pci = values.pci;
    console.log(this.pci);
    this.procedure = (Number(this.pci));
    console.log(typeof this.procedure);
    this.validationbtn = false;
    this.iswarning = false;
    this.iswarning1 = false;
    this.success = false;

  }

  dcstore(values) {
    console.log("hai");
    this.dc = values.dc;
    console.log(this.dc);
    this.diagnosis = (Number(this.dc));
    console.log(typeof this.diagnosis);
  }

  complete() {
    this._isCompleted = true;
  }
  servicecode(value){
    this.tos = value;
    this.typeofservice = this.tos;
  }
  placeofservicecode(value) {
    this.pos = value;
    this.placeofservice =  this.pos;
  }

  //patient validation



  /*
  onfunction(values){

    console.log(values.pcode);
    this.data=values.pcode;


    if(values.pcode!=='p1,p2') {


      this.sharedsevice.insertData(values.pcode+' procedure codes are not applicable for this patient.');
      //values.pcode='';
      this.valid=true;
    }
    else if(values.pcode=='p1,p2'){
      console.log("success");
      this.valid=false;
    }

  }





    onfunction1(values){
      console.log(values.fname);
      this.data1=values.fname;


      if(values.fname!=='Gopikrishna') {
        this.sharedsevice.insertData(values.fname+' name not matched for this Member id.Give Registered Name.');
        //values.pcode='';
        this.valid1=true;
      }
      else if(values.fname =='Gopikrishna'){
        console.log("success");
        this.valid1=false;
      }
      else if(values.fname==null) {
        this.valid1 = false;
      }
    }

    onfunction2(values){

      console.log(values.mid);
      this.data2=values.mid;


      if(values.mid!=='A123') {
        this.sharedsevice.insertData(values.mid+' Member id not Found.');
        //values.pcode='';
        this.valid2=true;
      }
      else if(values.mid=='A123'){
        console.log("success");
        this.valid2=false;
      }
    }

  */

}
