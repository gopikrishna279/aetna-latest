import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  ChangeDetectionStrategy,

  AfterContentInit, ViewChild, AfterViewChecked

} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { User } from '../user/user.model';
import { UsersService } from '../user/users.service';
import { Thread } from '../thread/thread.model';
import { ThreadsService } from '../thread/threads.service';
import { Message } from '../message/message.model';
import { MessagesService } from '../message/messages.service';


import { SharedService } from '../shared.service';

import * as $ from 'jquery';
import {isUndefined} from "util";

@Component({
  selector: 'chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css'],
  //changeDetection: ChangeDetectionStrategy.OnPush

})
export class ChatWindowComponent implements OnInit, AfterViewChecked {
  messages: Observable<any>;
  currentThread: Thread;
  draftMessage: Message;
  currentUser: User;
 start:string='Welcome to Athena health claim processing. Enter claim to start claim proccessing.'
  users:string[] =[];
  sender: string[] = [ ];
  valmsg:string[]=[];
  conflict1='Conflict: 52317 is the procedure code related to  kidney stones but diagnosis code 290.43 related to brain disease.';
  conflict2='Conflict: 87077 is the procedure code related to throat problems but diagnosis code 290.43 related to brain disease.';
timer;
public memberid: boolean = false;
public nameflag: boolean = false;
  public procedureflag: boolean = false;
  public moneyflag: boolean = false;
  public claimflag: boolean = false;
  @ViewChild("scroll") private chatWindowContainer: ElementRef;
  sendmsg;
  dmsg: any[] = [];
  public msg = '';
  singleArray = [];

public button: boolean = false;
public formval:boolean = false;

data = '';
  eermsg;

  text: '';
throatmsg="Related Throat problem : Superficial wounds, abscess, aspirates, animal bites. Skin biopsy should be submitted in a small amount of sterile, nonbacteriostatic saline."
  kidneymsg='52317: Kidney problem : Litholapaxy: crushing or fragmentation of calculus by any means in bladder and removal of fragments; simple or small (less than 2.5 cm)'

  diagonmsg= 'It denotes Vascular dementia';

  //data:string=[];


 public  frmmsg:boolean=false;

  info:string[]=['Gopi','A123','$500','$50'];



public name:string='Gopikrishna';
public memid:string='A123';
public money:string='$500';
public copay:string='$50';


public frmmsg1:string='sowmya';

public temp;
  constructor(public messagesService: MessagesService,
              public threadsService: ThreadsService,
              public UsersService: UsersService,
              public el: ElementRef,
              private router: Router, private sharedsevice: SharedService) {

}

 ngAfterViewChecked() {
  this.scrollToBottom();
}
 scrollToBottom(): void {
  this.chatWindowContainer.nativeElement.scrollTop= this.chatWindowContainer.nativeElement.scrollHeight;
}

  myCallback(){


   // this.formval=true;

    /*  this.data = this.sharedsevice.dataArray;
      console.log(this.data);
    this.valmsg.push(this.data);
    this.singleArray.push({
     user: this.valmsg.shift()});*/


     //this.valmsg = this.data;

     //this.singleArray.push({
     // user:this.valmsg.shift()});


      if (this.data == this.sharedsevice.data) {
      this.frmmsg =false;
    }
    else {
      this.frmmsg =false;
      this.eermsg = this.sharedsevice.data;


          this.valmsg.push(this.eermsg);
          this.singleArray.push({
              sys: this.valmsg.shift()
            }
          );


        if(this.eermsg == this.conflict1) {
          this.valmsg.push('As per our previous claim submission analysis, Claims with conflicts going to be rejected');
          this.singleArray.push({
              sys:this.valmsg.shift()
            }
          );
        }

        /*if(this.eermsg !== 'gopi') {
          this.valmsg.push('Hey, check your first name');
          this.singleArray.push({
              sys:this.valmsg.shift()
            }
          );
        }
*/

        if (this.eermsg == this.conflict2 ) {
          this.valmsg.push('As per our previous claim submission analysis, Claims with conflicts going to be rejected');
          this.singleArray.push({
              sys:this.valmsg.shift()
            }
          );
        }

        if(this.eermsg == 'no') {
          this.valmsg.push('90791:Psychiatric diagnostic evaluation');
          this.singleArray.push({
              sys: this.valmsg.shift()
            }
          );
          this.valmsg.push('90792:Psychiatric diagnostic evaluation with medical services');
          this.singleArray.push({
              sys: this.valmsg.shift()
            }
          );
          this.valmsg.push('90832:Psychotherapy, 30 minutes with patient and/or family member');
          this.singleArray.push({
              sys: this.valmsg.shift()
            }
          );
          this.valmsg.push('90833:Psychotherapy, 30 minutes with patient and/or family member when performed with an evaluation and management service');
          this.singleArray.push({
              sys: this.valmsg.shift()
            }
          );
          this.valmsg.push('90834:Psychotherapy, 45 minutes with patient and/or family member');
          this.singleArray.push({
              sys: this.valmsg.shift()
            }
          );
        }




      this.data = this.sharedsevice.data;
    }
    this.data = this.sharedsevice.data;
    console.log(this.data);

    //this.valmsg = this.data;

console.log(this.singleArray);

    }



  ngOnInit(): void {

    $('.btn-minimize').click(function(){
      $(this).toggleClass('btn-plus');
      $('.widget-content').slideToggle();


    });


   this.myCallback();

    this.timer = setInterval(() => {
      this.myCallback();
    }, 2000);


    /*this.data = this.sharedsevice.dataArray.shift();*/

    this.messages = this.threadsService.currentThreadMessages;

    this.draftMessage = new Message();

    this.threadsService.currentThread.subscribe(
      (thread: Thread) => {
        this.currentThread = thread;
      });

    this.UsersService.currentUser
      .subscribe(
        (user: User) => {
          this.currentUser = user;
        });

    this.messages
      .subscribe(
        (messages: Array<Message>) => {
          setTimeout(() => {
            this.scrollToBottom();
          });
        });
  }





  onEnter(value): void {


   this.frmmsg=true;

    /* this.data = this.sharedsevice.dataArray;
     console.log(this.data);

     this.frmmsg=this.data.shift();
     console.log(this.frmmsg);

*/
    if(value.search('profile') != -1){
      console.log(value);
      this.router.navigate(['/profile']);
      this.text = '';
      this.dmsg.push('Hey, you are in profile page.');
    }
   else if(value.search('home') != -1){
      this.router.navigate(['/home']);
      this.text = '';
      this.dmsg.push('Hey, you are in home page.');
    }
   else if(value.search('claim')!= -1){
      this.router.navigate(['/claim']);
      this.text = '';
      this.dmsg.push('Hey, you are in claim page. Start your claim submission by filling patient details.');
    }

   else if(value.search('87077') != -1){


      this.dmsg.push(this.throatmsg);
    }
    else if(value.search('52317') != -1){


      this.dmsg.push(this.kidneymsg);


    }
    else if(value.search('290.43') != -1){


      this.dmsg.push(this.diagonmsg);


    }

    else if (value.search('290.43') != -1 && value.search('icd10') ==-1 ){
      this.dmsg.push(this.diagonmsg);
    }

    else {
  this.msg = value;
  console.log(this.msg);
      //this.users.push(this.msg);

  console.log(this.users);
}
    this.users.push(value);
    this.sendmsg = this.sender.shift();
    this.singleArray.push({
      user:this.users.shift() ,  //this.users.shift()
      sys:this.dmsg.shift()     //this.dmsg.shift()
    });
    console.log(this.singleArray);

    this.text = '';

  }

  sendMessage(): void {
    const m: Message = this.draftMessage;
    m.author = this.currentUser;
    m.thread = this.currentThread;
    m.isRead = true;
    this.messagesService.addMessage(m);
    this.draftMessage = new Message();

  }

  /*scrollToBottom(): void {
    const scrollPane: any = this.el
      .nativeElement.querySelector('.msg-container-base');
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }*/
}
