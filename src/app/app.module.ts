import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import {appliedclaimsComponent } from './appliedclaims/appliedclaims';
import { UsersService } from './user/users.service';
import { ThreadsService } from './thread/threads.service';
import { MessagesService } from './message/messages.service';
import {ProfileComponent} from './profile/profile.component';
import { AppComponent } from './app.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { HomeComponent } from './home/home.component';
import { ChatimageComponent  } from './chat-image/chat-component';
import { SharedService } from './shared.service';
import {ClaimComponent} from './claim/claim.component';
import { RouterModule, Routes } from '@angular/router';
import { ChatThreadComponent } from './chat-thread/chat-thread.component';
import { ChatNavBarComponent } from './chat-nav-bar/chat-nav-bar.component';
import { ChatThreadsComponent } from './chat-threads/chat-threads.component';
import { ChatWindowComponent } from './chat-window/chat-window.component';
import { ChatPageComponent } from './chat-page/chat-page.component';
import { FromNowPipe } from './pipes/from-now.pipe';
//import { FormWizardModule } from 'angular2-wizard';
import { baWizzardComponent } from '../shared/baWizzard/baWizzard.component';
import { baWizzardStepComponent } from '../shared/baWizzard/baWizzardStep.component';


import { SideComponent } from './sidenav/sidenav';
const appRoutes: Routes = [
  {path: 'home',component:HomeComponent},
  {path:'claim',component:ClaimComponent},
  {path:'profile',component:ProfileComponent},
  //{path:'appliedclaims',component:appliedclaimsComponent},

  /*{path:'claim',component:ClaimComponent,children:[{
    path:'appliedclaims',component:appliedclaimsComponent, outlet:'aclaim'
  }]},*/





  {path:'', redirectTo:'/home', pathMatch:'full'}

];

@NgModule({
  declarations: [
    AppComponent,
    ChatMessageComponent,
    ChatThreadComponent,
    ChatNavBarComponent,
    ChatThreadsComponent,
    ChatWindowComponent,
    ChatPageComponent,
    FromNowPipe,
    HomeComponent,
    ClaimComponent,
    ProfileComponent,
    appliedclaimsComponent,
    ChatimageComponent,
    baWizzardComponent,
    baWizzardStepComponent,
    SideComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    //FormWizardModule,

    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    MessagesService, ThreadsService, UsersService, SharedService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
