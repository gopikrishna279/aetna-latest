import { Component, OnInit, Output, EventEmitter, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { baWizzardStepComponent } from './baWizzardStep.component';

import * as $ from 'jquery';
@Component({
  selector: 'baWizzard',
  styleUrls: ['./baWizzard.scss'],
  templateUrl: './baWizzard.html'
})
export class baWizzardComponent implements OnInit, AfterContentInit  {

  @ContentChildren(baWizzardStepComponent) wizardSteps: QueryList<baWizzardStepComponent>;
  private _steps: Array<baWizzardStepComponent> = [];
  private _isCompleted: boolean = false;

  @Output() onStepChanged: EventEmitter<baWizzardStepComponent> = new EventEmitter<baWizzardStepComponent>();
  constructor() {
    /*this.communicationService.componentMethodCalled$.subscribe(
      () => {
       /!* alert('(Component2) Method called!');*!/$('#next').click();
      }
    );
    this.communicationService.previousMethodCalled.subscribe(
      () => {
        $('#previous').click();
      }
    );*/
  }

  ngOnInit() {}

  ngAfterContentInit() {
    this.wizardSteps.forEach(step => this._steps.push(step));
    this._steps[0].isActive = true;
  }

  private get steps(): Array<baWizzardStepComponent> {
    return this._steps;
  }

  private get isCompleted(): boolean {
    return this._isCompleted;
  }

  private get activeStep(): baWizzardStepComponent {
    return this._steps.find(step => step.isActive);
  }

  private set activeStep(step: baWizzardStepComponent) {
    if (step !== this.activeStep && !step.isDisabled) {
      this.activeStep.isActive = false;
      step.isActive = true;
      this.onStepChanged.emit(step);
    }
  }

  private get activeStepIndex(): number {
    return this._steps.indexOf(this.activeStep);
  }

  private get hasNextStep(): boolean {
    return this.activeStepIndex < this._steps.length - 1;
  }

  private get hasPrevStep(): boolean {
    return this.activeStepIndex > 0;
  }

  goToStep(step: baWizzardStepComponent) {
    this.activeStep = step;
  }

  next() {
    if (this.hasNextStep) {
      let nextStep: baWizzardStepComponent = this._steps[this.activeStepIndex + 1];
      this.activeStep.onNext.emit();
      nextStep.isDisabled = false;
      this.activeStep = nextStep;
    }
  }

  previous() {
    if (this.hasPrevStep) {
      let prevStep: baWizzardStepComponent = this._steps[this.activeStepIndex - 1];
      this.activeStep.onPrev.emit();
      prevStep.isDisabled = false;
      this.activeStep = prevStep;
    }
  }

  complete() {
    this._isCompleted = true;
  }

}
